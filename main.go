// goxtempl is a simple program which expands a Go template and generate one output file
// It is used by the 'godsp' package to generate operators for several different types.
// It is normally invoked by 'go generate'.
//
// For example to generate Max() and Min() functions for several number types,
// create a template like the following in a file called 'maxmin.t'
//
// {{range .}}
// func Max{{.Name}}(a, b {{.Type}}) {{.Type}} {
//
//     if a > b {
//         return a
//     }
//     return b
// }
//
// func Min{{.Name}}(a, b {{.Type}}) {{.Type}} {
//
//     if a < b {
//         return a
//     }
//     return b
// }
// {{end}}
//
// Then, in the same directory creates a Go file with instructions for "go generate":
//
// go:generate goxtempl -in=maxmin.t -out=maxmin.go Name=Int,F32,F64 Type=int,float32,float64
//
// After executing "go generate" the "maxmin.go" will be created with 6 Max and Min functions
// for the specified Names and Types.
//
package main

import (
	"bytes"
	"flag"
	"fmt"
	"go/format"
	"io/ioutil"
	"os"
	"strings"
	"text/template"
)

const (
	progName = "GoXtempl"
	vMajor   = 0
	vMinor   = 1
)

// Command line options
var (
	oVersion = flag.Bool("version", false, "Show version and exits")
	oInp     = flag.String("in", "", "Input template file")
	oOut     = flag.String("out", "", "Output file")
)

func main() {

	// Parse command line parameters
	flag.Usage = usage
	flag.Parse()

	// If requested, print version and exits
	if *oVersion == true {
		fmt.Fprintf(os.Stderr, "%s v%d.%d\n", progName, vMajor, vMinor)
		return
	}

	// Try to open input file
	var fin *os.File
	var err error
	if *oInp == "" {
		fin = os.Stdin
	} else {
		fin, err = os.Open(*oInp)
		if err != nil {
			abort(err)
		}
	}

	// Opens output file
	var fout *os.File
	if *oOut == "" {
		fout = os.Stdout
	} else {
		fout, err = os.Create(*oOut)
		if err != nil {
			abort(err)
		}
	}

	// Parse name/value pairs
	names := make(map[string][]string)
	nvals := -1
	for _, nv := range flag.Args() {
		parts := strings.Split(nv, "=")
		// name/value pair must be in the form: name=value[,value[,value...]]]
		if len(parts) != 2 {
			err := fmt.Errorf("Invalid name/value pair:%s", nv)
			abort(err)
		}
		n := parts[0]
		names[n] = strings.Split(parts[1], ",")
		// All names must have the same number of values
		if nvals < 0 {
			nvals = len(names[n])
		} else {
			if nvals != len(names[n]) {
				err := fmt.Errorf("The number of values must the same for all names")
				abort(err)
			}
		}
	}

	// expand template
	err = expand(fin, fout, names)
	if err != nil {
		// Remove generated file
		if *oOut != "" {
			os.Remove(*oOut)
		}
		abort(err)
	}
}

// expand generates file from the input template to the output file using the names map
func expand(fin *os.File, fout *os.File, names map[string][]string) error {

	// Reads input file (template)
	text, err := ioutil.ReadAll(fin)
	if err != nil {
		return err
	}

	// Parses the template
	tmpl := template.New("tmpl")
	tmpl, err = tmpl.Parse(string(text))
	if err != nil {
		return err
	}

	// Create an array of maps from the input value/pair names:
	// from: map[n1:[v1,v2], n2:[v3,v4]]
	// to:   [map[n1:v1, n2:v3], map[n1:v2, n2:v4]]
	amaps := make([]map[string]string, 0)
	// Get the number of values per name
	var key string
	for k := range names {
		key = k
		break
	}
	// Generates the map list
	n := len(names[key])
	for i := 0; i < n; i++ {
		m := make(map[string]string)
		for k, v := range names {
			m[k] = v[i]
		}
		amaps = append(amaps, m)
	}

	// Expands template to buffer
	var buf bytes.Buffer
	err = tmpl.Execute(&buf, amaps)
	if err != nil {
		return err
	}

	// Formats buffer as Go source
	p, err := format.Source(buf.Bytes())
	if err != nil {
		return err
	}

	// Writes formatted source to output file
	fout.Write(p)
	fout.Close()
	return nil
}

// usage shows the application usage
func usage() {

	fmt.Fprintf(os.Stderr, "%s v%d.%d\n", progName, vMajor, vMinor)
	fmt.Fprintf(os.Stderr, "usage: %s [options] name1=value1,...,value2 name2=value1,...value2\n", strings.ToLower(progName))
	flag.PrintDefaults()
	os.Exit(2)
}

func abort(err error) {

	fmt.Fprintf(os.Stderr, "%s\n", err)
	os.Exit(1)
}
