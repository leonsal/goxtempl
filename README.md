`goxtempl` is a simple program which expands a Go template and writes to an output file.
It is used by the 'godsp' package to generate signal sources and nodes for several different Go types.
It is normally invoked by 'go generate'.

For example to generate Max() and Min() functions for several number types,
create a template like the following in a file called 'maxmin.t'

```
{{range .}}
func Max{{.Name}}(a, b {{.Type}}) {{.Type}} {

    if a > b {
        return a
    }
    return b
}

func Min{{.Name}}(a, b {{.Type}}) {{.Type}} {

    if a < b {
        return a
    }
    return b
}
{{end}}
```

Then, in the same directory creates a Go file with instructions for "go generate":

go:generate goxtempl -in=maxmin.t -out=maxmin.go Name=Int,F32,F64 Type=int,float32,float64

After executing "go generate" the "maxmin.go" will be created with 6 Max and Min functions
for the specified Names and Types.


