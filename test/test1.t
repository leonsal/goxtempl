package test

// File expanded from template:test1.t

{{range .}}
func Adder{{.Name}}(a, b []{{.Type}}) []{{.Type}} {

    out := make([]{{.Type}}, len(a))
    for i := 0; i < len(a); i++ {
        out[i] = a[i] + b[i]
    }
    return out
}
{{end}}

