package test

{{range .}}
type Input{{.Name}} struct {
    c chan {{.Type}}
}

func NewInput{{.Name}}() *Input{{.Name}} {

    inp := new(Input{{.Name}})
    inp.c = make(chan {{.Type}})
    return inp
}
{{end}}


