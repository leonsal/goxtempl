package test

//go:generate goxtempl -in=test1.t -out=gen_test1_f32.go Name=F32 Type=float32
//go:generate goxtempl -in=test1.t -out=gen_test1_f64.go Name=F64 Type=float64
//go:generate goxtempl -in=test1.t -out=gen_test1_cx.go  Name=C64,C128 Type=complex64,complex128
//go:generate goxtempl -in=test2.t -out=gen_test2_f64.go Name=F32,F64 Type=float32,float64
//go:generate goxtempl -in=maxmin.t -out=gen_maxmin.go Name=Int,F32,F64 Type=int,float32,float64
