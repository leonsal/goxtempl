package test

// File expanded from template:test1.t

{{range .}}
func Max{{.Name}}(a, b {{.Type}}) {{.Type}} {

    if a > b {
        return a
    }
    return b
}

func Min{{.Name}}(a, b {{.Type}}) {{.Type}} {

    if a < b {
        return a
    }
    return b
}
{{end}}

